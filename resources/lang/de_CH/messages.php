<?php

return [
	'ModelNotFoundException' => 'ModelNotFoundException wurde abgefangen. Das gewünschte Element wurde also nicht gefunden.',
	'ValidationException' => 'Bei der Validierung der Eingabe wurde ein Fehler festgestellt.',
	'ReferencedDataset' => 'Der gewünschte Vorgang konnte nicht durchgeführt werden, weil der Datensatz in der Datenbank noch referenziert wird.',
	'queryError' => 'Die gewünschte Datenbankabfrage konnte nicht durchgeführt werden. Es ist ein Fehler aufgetreten.',
	'generalError' => 'Es ist ein Fehler aufgetreten.',
];