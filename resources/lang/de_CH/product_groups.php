<?php

return [
	'fetchedAll' => 'Die Daten aller Produktgruppen wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Datensatz der gewünschten Produktgruppe wurde erfolgreich geladen.',
	'created' => 'Die Produktgruppe wurde erfolgreich erstellt.',
	'updated' => 'Die Produktgruppe wurde erfolgreich verändert.',
	'deleted' => 'Die Produktgruppe wurde erfolgreich gelöscht.',
	'addedProduct' => 'Das gewünschte Produkt wurde erfolgreich hinzugefügt.',
	'removedProduct' => 'Das gewünschte Produkt wurde erfolgreich entfernt.',
];