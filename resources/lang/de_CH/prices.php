<?php

return [
	'fetchedAll' => 'Die Daten aller Preise wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Preis des gewünschten Produktes wurde erfolgreich geladen.',
	'created' => 'Der Preis wurde erfolgreich erstellt.',
	'updated' => 'Der Preis wurde erfolgreich verändert.',
	'deleted' => 'Der Preis wurde erfolgreich gelöscht.'
];