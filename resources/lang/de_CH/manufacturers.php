<?php

return [
	'fetchedAll' => 'Die Daten aller Hersteller wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Datensatz des gewünschten Herstellers wurde erfolgreich geladen.',
	'created' => 'Der Hersteller wurde erfolgreich erstellt.',
	'updated' => 'Der Hersteller wurde erfolgreich verändert.',
	'deleted' => 'Der Hersteller wurde erfolgreich gelöscht.'
];