<?php

return [
	'fetchedAll' => 'Die Daten aller Läden wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Datensatz des gewünschten Ladens wurde erfolgreich geladen.',
	'created' => 'Der Laden wurde erfolgreich erstellt.',
	'updated' => 'Der Laden wurde erfolgreich verändert.',
	'deleted' => 'Der Laden wurde erfolgreich gelöscht.'
];