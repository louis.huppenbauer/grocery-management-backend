<?php

return [
	'fetchedAll' => 'Die Daten aller Produkte wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Datensatz des gewünschten Produktes wurde erfolgreich geladen.',
	'created' => 'Das Produkt wurde erfolgreich erstellt.',
	'updated' => 'Das Produkt wurde erfolgreich verändert.',
	'deleted' => 'Das Produkt wurde erfolgreich gelöscht.',
];