<?php

return [
	'fetchedAll' => 'Die Daten aller Kategorien wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Datensatz der gewünschten Kategorie wurde erfolgreich geladen.',
	'created' => 'Die Kategorie wurde erfolgreich erstellt.',
	'updated' => 'Die Kategorie wurde erfolgreich verändert.',
	'deleted' => 'Die Kategorie wurde erfolgreich gelöscht.'
];