<?php

return [
	'fetchedAll' => 'Die Daten aller Länder wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Datensatz des gewünschten Landes wurde erfolgreich geladen.',
	'created' => 'Das Land wurde erfolgreich erstellt.',
	'updated' => 'Das Land wurde erfolgreich verändert.',
	'deleted' => 'Das Land wurde erfolgreich gelöscht.'
];