<?php

return [
	'fetchedAll' => 'Die Daten aller Währungen wurden erfolgreich geladen.',
	'fetchedSpecific' => 'Der Datensatz der gewünschten Währung wurde erfolgreich geladen.',
	'created' => 'Die Währung wurde erfolgreich erstellt.',
	'updated' => 'Die Währung wurde erfolgreich verändert.',
	'deleted' => 'Die Währung wurde erfolgreich gelöscht.'
];