<?php
return [
	'required' => 'Das Feld :attribute muss ausgefüllt sein.',
	'unique' => 'Der Wert im Feld :attribute existiert bereits. Bitte wählen Sie einen anderen.',
    'max' => [
        'string' => 'Der Wert im Feld :attribute darf nicht länger als :max Zeichen sein.',
    ],
    'exists' => 'Der Wert im Feld :attribute scheint eigentlich gar nicht zu existieren.',
    'boolean' => 'Das Feld :attribute kann nur mit Ja oder Nein abgefüllt werden.',
    'date'    => 'Das Feld :attribute beinhaltet kein korrektes Datum.',
    'date_format' => 'Das Feld :attribute beinhaltet kein korrektes Datum.',
    'numeric' => 'Das Feld :attribute ist kein korrekter Zahlenwert.',
    'custom' => [
        /*'name' => [
        	'required' => 'Der Name muss ausgefüllt sein!',
        ],*/
    ],

    'attributes' => [
    	'name' => 'Name',
        'shortcode' => 'Kürzel',
        'location'  => 'Standort',
        'country_id'=> 'Land',
        'user_id'   => 'Benutzer',
        'category_id' => 'Kategorie',
        'store_id' => 'Laden',
        'manufacturer_id' => 'Hersteller',
        'product_group_id' => 'Produktgruppe',
        'product_id' => 'Produkt',
        'date' => 'Datum',
        'comment' => 'Kommentar',
        'isBio' => 'Bio',
        'volume' => 'Volumen',
        'weight' => 'Gewicht',
        'cost' => 'Preis',
        'isBargain' => 'Aktionsangebot',
    ],
];