<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Transformers\CountryTransformer;

class CountryController extends APIController
{
    public static $currentModel = 'App\Models\Country';
    protected     $relToLoad    = array();

    protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return [
                    'country' => 'required',
                    'country.name'      => 'required|unique:countries,name|max:255',
                    'country.shortcode' => 'required|unique:countries,shortcode|max:3',
                ];
            break;

            case 'onUpdate':
                $id = $request->route()[2]['id'];

                return [
                    'country' => 'required',
                    'country.name' => [
                        'required',
                        'max:255',
                        Rule::unique('countries', 'name')->ignore($id),
                    ],
                    'country.shortcode' => [
                        'required',
                        'max:3',
                        Rule::unique('countries', 'shortcode')->ignore($id),
                    ],
                ];
            break;
        }
    }

    protected function show($id, $relToLoad = array()) {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new CountryTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');

            $fractal = app('fractal')->item($dataset, new CountryTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }

    public function deleteStore($id, $store) {
        // get product group
        $country_dataset = self::$currentModel::findOrFail($id)->load('stores');

        // get product from product group
        $store_dataset = $country_dataset->stores()->findOrFail($store);

        //remove relation
        $store_dataset->country()->dissociate()->save();

        return response()->respond($store_dataset, self::$currentModel, 200, __(self::$currentModel::getTableName().'.removedProduct'));
    }
}
