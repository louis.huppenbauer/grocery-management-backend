<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Transformers\StoreTransformer;

class StoreController extends APIController
{
    public static   $currentModel = 'App\Models\Store';
    protected       $relToLoad    = array();

   protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return  [
                    'store' => 'required',
                    'store.name' => 'required|max:255',
                    'store.location' => 'required|max:255',
                    //'store.country_id' => 'required|exists:countries,id',
                ];
            break;

            case 'onUpdate': 
                $id = $request->route()[2]['id'];

                return  [
                    'store' => 'required',
                    'store.name' => 'required|max:255',
                    'store.location' => 'required|max:255',
                    //'store.country_id' => 'required|exists:countries,id',
                ];
            break;
        }
    }

    protected function show($id, $relToLoad = array())
    {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new StoreTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');
            
            $fractal = app('fractal')->item($dataset, new StoreTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }
}