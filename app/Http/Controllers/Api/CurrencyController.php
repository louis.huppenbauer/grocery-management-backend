<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Transformers\CurrencyTransformer;

class CurrencyController extends APIController
{
    public static $currentModel = 'App\Models\Currency';
    protected     $relToLoad    = array();   

    protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return [
                    'currency' => 'required',
                    'currency.name'      => 'required|unique:currencies,name|max:255',
                    'currency.shortcode' => 'required|unique:currencies,shortcode|max:3',
                ];
            break;

            case 'onUpdate':
                $id = $request->route()[2]['id'];

                return [
                    'currency' => 'required',
                    'currency.name' => [
                        'required',
                        'max:255',
                        Rule::unique('currencies', 'name')->ignore($id),
                    ],
                    'currency.shortcode' => [
                        'required',
                        'max:3',
                        Rule::unique('currencies', 'shortcode')->ignore($id),
                    ],
                ];
            break;
        }
    }

    protected function show($id, $relToLoad = array()) {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new CurrencyTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');
            
            $fractal = app('fractal')->item($dataset, new CurrencyTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }
}