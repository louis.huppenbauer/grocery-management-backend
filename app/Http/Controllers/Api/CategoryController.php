<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Transformers\CategoryTransformer;

class CategoryController extends APIController
{
    public static $currentModel = 'App\Models\Category';
    protected     $relToLoad    = array();

    protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return  [
                    'category'      => 'required',
                    'category.name' => 'required|unique:categories,name|max:255'
                ];
            break;

            case 'onUpdate':
                $id = $request->route()[2]['id'];

                return [
                    'category'      => 'required',
                    'category.name' => [
                        'required',
                        'max:255',
                        Rule::unique('categories', 'name')->ignore($id),
                    ],        
                ];
            break;

        }
    }

     protected function show($id, $relToLoad = array()) {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new CategoryTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');
            
            $fractal = app('fractal')->item($dataset, new CategoryTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }
}