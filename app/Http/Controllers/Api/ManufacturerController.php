<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Transformers\ManufacturerTransformer;

class ManufacturerController extends APIController
{
    public static   $currentModel = 'App\Models\Manufacturer';
    protected       $relToLoad    = array();

    protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return  [
                    'manufacturer' => 'required',
                    'manufacturer.name' => 'required|unique:manufacturers,name|max:255'
                ];
            break;

            case 'onUpdate':
                $id = $request->route()[2]['id'];

                return [
                    'manufacturer' => 'required',
                    'manufacturer.name' => [
                        'required',
                        'max:255',
                        Rule::unique('manufacturers', 'name')->ignore($id),
                    ],        
                ];
            break;

        }
    }

    protected function show($id, $relToLoad = array()) {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new ManufacturerTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');
            
            $fractal = app('fractal')->item($dataset, new ManufacturerTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }
}