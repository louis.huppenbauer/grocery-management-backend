<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Transformers\PriceTransformer;

class PriceController extends APIController
{
    public static $currentModel = 'App\Models\Price';
    protected     $relToLoad  = array();   

    protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return [
                    'price' => 'required',
                    'price.user_id' => 'required|integer|exists:users,id',
                    'price.cost' => 'required|numeric',
                    'price.volume' => 'required_without:weight|numeric',
                    'price.weight'  => 'required_without:volume|numeric',
                    'price.isBargain' => 'required|boolean',
                    'price.currency_id'  => 'integer|exists:product_groups,id',
                    'price.product_id'   => 'integer|unique:prices,product_id|exists:manufacturers,id',
                ];
            break;

            case 'onUpdate':
                $id = $request->route()[2]['id'];

                return [
                    'price' => 'required',                    
                    'price.user_id' => 'required|integer|exists:users,id',
                    'price.cost' => 'required|numeric',
                    'price.volume'  => 'required_without:weight|numeric',
                    'price.weight'  => 'required_without:volume|numeric',
                    'price.isBargain' => 'required|boolean',
                    'price.currency_id'  => 'integer|exists:product_groups,id',
                    'price.product_id'   => 'integer|unique:prices,product_id|exists:manufacturers,id',
                ];
            break;
        }
    }

    protected function show($id, $relToLoad = array()) {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new PriceTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');
            
            $fractal = app('fractal')->item($dataset, new PriceTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }
}