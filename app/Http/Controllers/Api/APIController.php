<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

abstract class APIController extends Controller 
{
	public static $currentModel;
	protected $relToLoad;
	protected $validationRules;

	public function showAll() {
		return $this->show(0, $this->relToLoad);
	}

	public function showOne($id) {
		return $this->show($id, $this->relToLoad);	
	}

	protected function show($id, $relToLoad = array())
    {
    	if($id === 0) {
    		$dataset = static::$currentModel::all();
    		$message = __(static::$currentModel::getTableName().'.fetchedAll');
    	}
    	else {
    		$dataset = static::$currentModel::findOrFail($id);
    		$message = __(static::$currentModel::getTableName().'.fetchedSpecific');
    	}

    	if(!empty($relToLoad)) {
    		$dataset = $dataset->load($relToLoad);
    	}

        // eager load our relationship (so we see it in a dump)
        return response()->respond($dataset, static::$currentModel, 200, $message);
    }
    
	public function create(Request $request)
    {
    	$rules = $this->getValidationRules('onCreate', $request);

    	if(!empty($rules)) {
        	$this->validate($request, $rules);
        }

        $explodedModel = explode('\\', static::$currentModel);
        $modelName     = strtolower(end($explodedModel));

        $dataset = static::$currentModel::create($request->input($modelName));

        return response()->respond($dataset, static::$currentModel, 201, __(static::$currentModel::getTableName().'.created'));
    }


	public function update($id, Request $request)
    {
    	$rules = $this->getValidationRules('onUpdate', $request);
        $dataset = static::$currentModel::findOrFail($id);
        
        if(!empty($rules)) {
        	$this->validate($request, $rules);
        }
 
        $explodedModel = explode('\\', static::$currentModel);
        $modelName     = strtolower(end($explodedModel));

        $dataset->update($request->input($modelName));

        return response()->respond($dataset, static::$currentModel, 200, __(static::$currentModel::getTableName().'.updated'));
    }
	

	public function delete($id)
    {
        static::$currentModel::findOrFail($id)->delete();
        return response()->respond([], static::$currentModel, 200, __(static::$currentModel::getTableName().'.deleted'));
    }

    abstract protected function getValidationRules($type, Request $request);
}