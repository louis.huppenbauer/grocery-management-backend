<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Product;
use App\Http\Transformers\ProductGroupTransformer;

class ProductGroupController extends APIController
{

    public static $currentModel = 'App\Models\ProductGroup';
    protected     $relToLoad  = array();   

    protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return [
                    'product_group' => 'required',
                    'product_group.name'        => 'required|unique:product_groups,name|max:255',
                    'product_group.user_id'     => 'required|integer|exists:users,id',
                    'product_group.category_id' => 'required|integer|exists:categories,id',
                ];
            break;

            case 'onUpdate':
                $id = $request->route()[2]['id'];

                return [
                    'product_group' => 'required',
                    'product_group.name' => [
                        'required',
                        'max:255',
                        Rule::unique('product_groups', 'name')->ignore($id),
                    ],
                    'product_group.user_id'     => 'required|integer|exists:users,id',
                    'product_group.category_id' => 'required|integer|exists:categories,id',
                ];
            break;
        }
    }

    protected function show($id, $relToLoad = array()) {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new ProductGroupTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');
            
            $fractal = app('fractal')->item($dataset, new ProductGroupTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }

    public function addProduct($id, $product) {

       // get product group
        $product_group_dataset = self::$currentModel::findOrFail($id);

        // get product
        $product_dataset = Product::findOrFail($product);

         //add relation
        $product_group_dataset->products()->save($product_dataset);

        return response()->respond($product_group_dataset->load($this->relToLoad), self::$currentModel, 200, __(self::$currentModel::getTableName().'.addedProduct'));
    }

    public function deleteProduct($id, $product) {        
        // get product group
        $product_group_dataset = self::$currentModel::findOrFail($id)->load('products');

        // get product from product group
        $product_dataset = $product_group_dataset->products()->findOrFail($product);
        
        //remove relation
        $product_dataset->product_group()->dissociate()->save();

        return response()->respond($product_dataset, self::$currentModel, 200, __(self::$currentModel::getTableName().'.removedProduct'));
    }
}