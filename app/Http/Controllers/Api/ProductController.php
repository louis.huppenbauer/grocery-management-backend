<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Transformers\ProductTransformer;

class ProductController extends APIController
{
    public static $currentModel = 'App\Models\Product';
    protected     $relToLoad  = array();   

    protected function getValidationRules($type, Request $request) {
        switch($type) {
            case 'onCreate':
                return [
                    'product' => 'required',
                    'product.name'    => 'required|String|unique:products,name|max:255',
                    'product.user_id' => 'required|integer|exists:users,id',
                    'product.isBio'   => 'required|boolean',
                    'product.comment' => 'String',
                    'product.date'              => 'date_format:Y-m-d H:i:s',
                    'product.product_group_id'  => 'integer|exists:product_groups,id',
                    'product.manufacturer_id'   => 'integer|exists:manufacturers,id',
                    'product.store_id'          => 'integer|exists:stores,id',
                    'product.country_id'        => 'integer|exists:countries,id',
                ];
            break;

            case 'onUpdate':
                $id = $request->route()[2]['id'];

                return [
                    'product' => 'required',
                    'product.name' => [
                        'required',
                        'max:255',
                        Rule::unique('products', 'name')->ignore($id),
                    ],
                    'product.user_id' => 'required|integer|exists:users,id',
                    'product.isBio'   => 'required|boolean',
                    'product.comment' => 'String',
                    'product.date'              => 'date_format:Y-m-d H:i:s',
                    'product.product_group_id'  => 'integer|exists:product_groups,id',
                    'product.manufacturer_id'   => 'integer|exists:manufacturers,id',
                    'product.store_id'          => 'integer|exists:stores,id',
                    'product.country_id'        => 'integer|exists:countries,id',
                ];
            break;
        }
    }

    protected function show($id, $relToLoad = array()) {
        if (isset($_GET['include'])) {
            app('fractal')->includes($_GET['include']);
        }

        if($id === 0) {
            $dataset = static::$currentModel::all();
            $message = __(static::$currentModel::getTableName().'.fetchedAll');

            $fractal = app('fractal')->collection($dataset, new ProductTransformer())->getArray();
        }
        else {
            $dataset = static::$currentModel::findOrFail($id);
            $message = __(static::$currentModel::getTableName().'.fetchedSpecific');
            
            $fractal = app('fractal')->item($dataset, new ProductTransformer())->getArray();
        }

        // eager load our relationship (so we see it in a dump)
        return response()->respond($fractal, static::$currentModel, 200, $message);
    }

}