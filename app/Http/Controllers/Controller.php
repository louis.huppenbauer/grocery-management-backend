<?php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{

	protected function buildFailedValidationResponse(\Illuminate\Http\Request $request, array $errors) {
		$error 				= array();
		$error['type'] 		= 'ValidationException';
		$error['status'] 	= 422;
        $error['debugInfo'] = $errors;
        $error['message']	= __('messages.ValidationException');

        return response()->respond([], 'errors', $error['status'], $error['message'], $error);
	}
}

