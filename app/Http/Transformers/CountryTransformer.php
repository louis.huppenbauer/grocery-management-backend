<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\StoreTransformer;
use App\Http\Transformers\ProductTransformer;

use App\Models\Country;

class CountryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'stores',
        'currencies',
        'products',
    ];

    protected $defaultIncludes = [

    ];

    /**
     * Turn Country object into a generic array.
     *
     * @return array
     */
    public function transform(Country $dataset)
    {
        return [
            'id' => $dataset->id,
            'name' => $dataset->name,
            'shortcode' => $dataset->shortcode,
            'currencies' => $dataset->currencies->pluck('id'),
            'stores' => $dataset->stores->pluck('id'),
            //'products' => $dataset->products->pluck('id'),
        ];
    }

     /**
     * Include Currencies
     *
     * @param Country $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeStores(Country $dataset)
    {
        $extractedSet = $dataset->stores;
        return $this->collection($extractedSet, new StoreTransformer);
    }

     /**
     * Include Currencies
     *
     * @param Country $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCurrencies(Country $dataset)
    {
        $extractedSet = $dataset->currencies;
        return $this->collection($extractedSet, new CurrencyTransformer);
    }

    /**
     * Include products
     *
     * @param Country $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProducts(Country $dataset)
    {
        $extractedSet = $dataset->products;
        return $this->collection($extractedSet, new ProductTransformer);
    }
}
