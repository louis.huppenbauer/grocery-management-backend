<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\CountryTransformer;
use App\Http\Transformers\PriceTransformer;

use App\Models\Currency;

class CurrencyTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
         'countries',
         'prices',
    ];

    protected $defaultIncludes = [

    ];

    /**
     * Turn Currency object into a generic array.
     *
     * @return array
     */
    public function transform(Currency $dataset)
    {
        return [
            'id' => $dataset->id,
            'name' => $dataset->name,
            'shortcode' => $dataset->shortcode,
            'countries' => $dataset->countries->pluck('id'),
            //'prices' => $dataset->prices->pluck('id'),
        ];
    }

    /**
     * Include Countries
     *
     * @param Currency $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCountries(Currency $dataset)
    {
        $extractedSet = $dataset->countries;
        return $extractedSet ? $this->collection($extractedSet, new CountryTransformer) : null;
    }

    /**
     * Include prices
     *
     * @param Currency $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includePrices(Currency $dataset)
    {
        $extractedSet = $dataset->prices;
        return $extractedSet ? $this->collection($extractedSet, new PriceTransformer) : null;
    }
}
