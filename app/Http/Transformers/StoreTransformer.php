<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\CountryTransformer;
use App\Http\Transformers\ProductTransformer;

use App\Models\Store;

class StoreTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'country',
        'products',
    ];

    protected $defaultIncludes = [
    ];

    /**
     * Turn Store object into a generic array.
     *
     * @return array
     */
    public function transform(Store $dataset)
    {
        return [
            'id' => $dataset->id,
            'name' => $dataset->name,
            'location' => $dataset->location,
            //It should be enough to just receive the country id. If it seems i would need the whole objects -> look at frontend, embeddedserializer for store
            'country' => $dataset->country ? $dataset->country->id : 0,
            //'products' => !$dataset->products->isEmpty() ? $dataset->products->pluck('id') : [],
        ];
    }

    /**
     * Include Products
     *
     * @param Store $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCountry(Store $dataset)
    {
        $extractedSet = $dataset->country;
        return $extractedSet ? $this->item($extractedSet, new CountryTransformer) : null;
    }

    /**
     * Include Products
     *
     * @param Store $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProducts(Store $dataset)
    {
        $extractedSet = $dataset->products;
        return $extractedSet ? $this->collection($extractedSet, new ProductTransformer) : null;
    }
}
