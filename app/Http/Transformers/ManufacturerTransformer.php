<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\ProductTransformer;

use App\Models\Manufacturer; 

class ManufacturerTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
         'products',
    ];

    protected $defaultIncludes = [
       
    ];

    /**
     * Turn Manufacturer object into a generic array.
     *
     * @return array
     */
    public function transform(Manufacturer $dataset)
    {
        return [
            'id' => $dataset->id,
            'name' => $dataset->name,
            //'products' => $dataset->products->pluck('id'),
        ];
    }

    /**
     * Include Products
     *
     * @param Manufacturer $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProducts(Manufacturer $dataset)
    {
        $extractedSet = $dataset->products;
        return $extractedSet ? $this->collection($extractedSet, new ProductTransformer) : null;
    }
}