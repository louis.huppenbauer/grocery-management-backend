<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\ProductGroupTransformer;

use App\Models\Category; 

class CategoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'productGroups',
    ];

    protected $defaultIncludes = [
       
    ];

    /**
     * Turn Category object into a generic array.
     *
     * @return array
     */
    public function transform(Category $dataset)
    {
        return [
            'id' => $dataset->id,
            'name' => $dataset->name,
            //'productGroups' => $dataset->productGroups->pluck('id'),
        ];
    }

    /**
     * Include productGroups
     *
     * @param Category $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProductGroups(Category $dataset)
    {
        $extractedSet = $dataset->productGroups;
        return $extractedSet ? $this->collection($extractedSet, new ProductGroupTransformer) : null;
    }
}