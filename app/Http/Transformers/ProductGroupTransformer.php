<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\CategoryTransformer;
use App\Http\Transformers\ProductTransformer;

use App\Models\ProductGroup; 

class ProductGroupTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'products',
        'category',
    ];

    protected $defaultIncludes = [
       
    ];

    /**
     * Turn User object into a generic array.
     *
     * @return array
     */
    public function transform(ProductGroup $dataset)
    {
        return [
            'id' => $dataset->id,
            'name' => $dataset->name,
            //'products' => $dataset->products->pluck('id'),
            //'category' => $dataset->category->id,
        ];
    }

    /**
     * Include productGroups
     *
     * @param ProductGroup $dataset
     * @return \League\Fractal\Resource\Item
     */
    public function includeCategory(ProductGroup $dataset)
    {
        $extractedSet = $dataset->category;
        return $extractedSet ? $this->item($extractedSet, new CategoryTransformer) : null;
    }


    /**
     * Include products
     *
     * @param ProductGroup $dataset
     * @return \League\Fractal\Resource\Item
     */
    public function includeProducts(ProductGroup $dataset)
    {
        $extractedSet = $dataset->products;
        return $extractedSet ? $this->collection($extractedSet, new ProductTransformer) : null;
    }
}