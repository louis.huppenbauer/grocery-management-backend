<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\ProductTransformer;
use App\Http\Transformers\CurrencyTransformer;

use App\Models\Price; 

class PriceTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'product',
        'currency',
    ];

    protected $defaultIncludes = [
       
    ];

    /**
     * Turn Price object into a generic array.
     *
     * @return array
     */
    public function transform(Price $dataset)
    {
        return [
            'id' => $dataset->id,            
            'cost' => $dataset->cost,
            'volume' => $dataset->volume,
            'weight' => $dataset->weight,
            'costPerVolume' => $dataset->costPerVolume,
            'costPerWeight' => $dataset->costPerWeight,
            'isBargain' => $dataset->isBargain,
            //'product' => $dataset->product->id,
            //'currency' => $dataset->currency->id,
        ];
    }

    /**
     * Include Currency
     *
     * @param Price $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeCurrency(Price $dataset)
    {
        $extractedSet = $dataset->product;
        return $extractedSet ? $this->collection($extractedSet, new CurrencyTransformer) : null;
    }

    /**
     * Include Product
     *
     * @param Price $dataset
     * @return \League\Fractal\Resource\Collection
     */
    public function includeProduct(Price $dataset)
    {
        $extractedSet = $dataset->currency;
        return $extractedSet ? $this->collection($extractedSet, new ProductTransformer) : null;
    }
}