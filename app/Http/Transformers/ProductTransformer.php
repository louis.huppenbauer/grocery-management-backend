<?php

namespace App\Http\Transformers;

use League\Fractal\TransformerAbstract;
use App\Http\Transformers\ManufacturerTransformer;
use App\Http\Transformers\StoreTransformer;
use App\Http\Transformers\CountryTransformer;
use App\Http\Transformers\ProductGroupTransformer;

use App\Models\Product; 

class ProductTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'manufacturer',
        'store',
        'country',
        'price',
        'productGroup',
    ];

    protected $defaultIncludes = [
       
    ];

    /**
     * Turn Product object into a generic array.
     *
     * @return array
     */
    public function transform(Product $dataset)
    {

        return [
            'id' => $dataset->id,
            'name' => $dataset->name,
            'isBio' => $dataset->isBio,
            'comment' => $dataset->comment,
            'date' => $dataset->date,
            //'manufacturer' =>  $dataset->manufacturer ? $dataset->manufacturer->id : 0,
            //'store' =>  $dataset->store ? $dataset->store->id : 0,
            //'country' =>  $dataset->country ? $dataset->country->id : 0,
            //'price' => $dataset->price ? $dataset->price->id : 0,
            //'productGroup' =>  $dataset->product_group ? $dataset->product_group->id : 0,
        ];
    }

    /**
     * Include manufacturer
     *
     * @param Product $dataset
     * @return \League\Fractal\Resource\Item
     */
    public function includeManufacturer(Product $dataset)
    {
        $extractedSet = $dataset->manufacturer;
        return $extractedSet ? $this->item($extractedSet, new ManufacturerTransformer) : 0;
    }

    /**
     * Include store
     *
     * @param Product $dataset
     * @return \League\Fractal\Resource\Item
     */
    public function includeStore(Product $dataset)
    {
        $extractedSet = $dataset->store;
        return $extractedSet ? $this->item($extractedSet, new StoreTransformer) : 0;
    }

    /**
     * Include country
     *
     * @param Product $dataset
     * @return \League\Fractal\Resource\Item
     */
    public function includeCountry(Product $dataset)
    {
        $extractedSet = $dataset->country;
        return $extractedSet ? $this->item($extractedSet, new CountryTransformer) : 0;
    }

    /**
     * Include productGroup
     *
     * @param Product $dataset
     * @return \League\Fractal\Resource\Item
     */
    public function includeProductGroup(Product $dataset)
    {
        $extractedSet = $dataset->product_group;
        return $extractedSet ? $this->item($extractedSet, new ProductGroupTransformer) : 0;
    }


    /**
     * Include price
     *
     * @param Product $dataset
     * @return \League\Fractal\Resource\Item
     */
    public function includePrice(Product $dataset)
    {   
        $extractedSet = $dataset->price;
        return $extractedSet ? $this->item($extractedSet, new PriceTransformer) : null;
    }
}