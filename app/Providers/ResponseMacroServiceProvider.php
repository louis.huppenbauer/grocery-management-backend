<?php
namespace App\Providers;

use Laravel\Lumen\Providers\EventServiceProvider as ServiceProvider;

class ResponseMacroServiceProvider extends ServiceProvider
{
  public function boot()
  {
    response()::macro('respond', function ($data, $model, $status = 200, $message = '', $error = [], $headers = []) {
    	/*$responseData = [
    		'status' => $status,
    		'errors' => $error,
    		'message' => $message,
    		'data' => $data,
    	];*/

      $explodedModel = explode('\\', $model);
      $modelName = end($explodedModel);

      $responseData = [
        $modelName => $data,
      ];

      if(sizeof($error)) {
        $responseData['errorData'] = $error;

        // this is to adhere to emberdata / rest standarts. it should be just temporary.
        $responseData['errors'][]['name'] = $error['message'];
      }

        return response()->json($responseData, $status, $headers);
    });
  }
}