<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property string $name
 * @property Category $category
 * @property User $user
 * @property Product[] $products
 */
class ProductGroup extends Model
{
    use EloquentGetTableName;

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'product_groups';

    /**
     * @var array
     */
    protected $fillable = ['category_id', 'user_id', 'name'];

    /**
     *  @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
