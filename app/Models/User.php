<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property string $email
 * @property Price[] $prices
 * @property ProductGroup[] $productGroups
 * @property Product[] $products
 */
class User extends Model
{
    use EloquentGetTableName;

    /**
     * @var array
     */
    protected $fillable = ['email'];

    /**
     *  @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany('App\Models\Price');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productGroups()
    {
        return $this->hasMany('App\Models\ProductGroup');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
