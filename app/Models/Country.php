<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property string $name
 * @property string $shortcode
 * @property Currency[] $currencies
 * @property Store[] $stores
 * @property Product[] $products
 */
class Country extends Model 
{
    use EloquentGetTableName;

    /**
     * @var array
     */
    protected $fillable = ['name', 'shortcode'];

    /**
     *  @var boolean
     */
    public $timestamps = false;
    

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function currencies()
    {
        return $this->belongsToMany('App\Models\Currency', 'currenciesXcountries');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function stores()
    {
        return $this->hasMany('App\Models\Store');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
