<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property string $name
 * @property string $shortcode
 * @property Country[] $countries
 * @property Price[] $prices
 */
class Currency extends Model
{
    use EloquentGetTableName;

    /**
     * @var array
     */
    protected $fillable = ['name', 'shortcode'];

    /**
     *  @var boolean
     */
    public $timestamps = false;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function countries()
    {
        return $this->belongsToMany('App\Models\Country', 'currenciesXcountries');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prices()
    {
        return $this->hasMany('App\Models\Price');
    }
}
