<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property string $name
 * @property ProductGroup[] $productGroups
 */
class Category extends Model
{
    use EloquentGetTableName;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     *  @var boolean
     */
    public $timestamps = false;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productGroups()
    {
        return $this->hasMany('App\Models\ProductGroup');
    }
}
