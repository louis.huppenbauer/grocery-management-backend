<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property float $cost
 * @property float $volume
 * @property float $weight
 * @property float $costPerVolume
 * @property float $costPerWeight
 * @property boolean $isBargain
 * @property Currency $currency
 * @property User $user
 * @property Product[] $product
 */
class Price extends Model
{
    use EloquentGetTableName;

    /**
     * @var array
     */
    protected $fillable = ['cost', 'volume', 'weight', 'costPerVolume', 'costPerWeight', 'isBargain'];

    /**
     *  @var boolean
     */
    public $timestamps = false;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}
