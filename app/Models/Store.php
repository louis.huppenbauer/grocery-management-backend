<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property string $name
 * @property string $location
 * @property Product[] $products
 * @property Country $country
 */
class Store extends Model
{
    use EloquentGetTableName;

    /**
     * @var array
     */
    protected $fillable = ['name', 'location', 'country_id'];

    /**
     *  @var boolean
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
