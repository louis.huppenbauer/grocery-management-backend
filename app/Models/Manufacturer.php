<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\EloquentGetTableName;

/**
 * @property int $id
 * @property string $name
 * @property Product[] $products
 */
class Manufacturer extends Model 
{
    use EloquentGetTableName;

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     *  @var boolean
     */
    public $timestamps = false;
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }
}
