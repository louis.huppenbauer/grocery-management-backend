<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api', 'namespace' => 'Api'], function () use ($router) {

    $router->group(['prefix' => 'countries'], function() use ($router) {
        $router->get('',  ['uses' => 'CountryController@showAll']);
        $router->get('{id}', ['uses' => 'CountryController@showOne']);
        $router->post('', ['uses' => 'CountryController@create']);
        $router->delete('{id}', ['uses' => 'CountryController@delete']);
        $router->put('{id}', ['uses' => 'CountryController@update']);
        $router->group(['prefix' => '{id}/store'], function() use ($router) {
            $router->delete('{store}', ['uses' => 'CountryController@deleteProduct']);
        });
    });

    $router->group(['prefix' => 'currencies'], function() use ($router) {
        $router->get('',  ['uses' => 'CurrencyController@showAll']);
        $router->get('{id}', ['uses' => 'CurrencyController@showOne']);
        $router->post('', ['uses' => 'CurrencyController@create']);
        $router->delete('{id}', ['uses' => 'CurrencyController@delete']);
        $router->put('{id}', ['uses' => 'CurrencyController@update']);
    });

    $router->group(['prefix' => 'categories'], function() use ($router) {
        $router->get('',  ['uses' => 'CategoryController@showAll']);
        $router->get('{id}', ['uses' => 'CategoryController@showOne']);
        $router->post('', ['uses' => 'CategoryController@create']);
        $router->delete('{id}', ['uses' => 'CategoryController@delete']);
        $router->put('{id}', ['uses' => 'CategoryController@update']);
    });

    $router->group(['prefix' => 'manufacturers'], function() use ($router) {
        $router->get('',  ['uses' => 'ManufacturerController@showAll']);
        $router->get('{id}', ['uses' => 'ManufacturerController@showOne']);
        $router->post('', ['uses' => 'ManufacturerController@create']);
        $router->delete('{id}', ['uses' => 'ManufacturerController@delete']);
        $router->put('{id}', ['uses' => 'ManufacturerController@update']);
    });

    $router->group(['prefix' => 'stores'], function() use ($router) {
        $router->get('',  ['uses' => 'StoreController@showAll']);
        $router->get('{id}', ['uses' => 'StoreController@showOne']);
        $router->post('', ['uses' => 'StoreController@create']);
        $router->delete('{id}', ['uses' => 'StoreController@delete']);
        $router->put('{id}', ['uses' => 'StoreController@update']);
    });

    $router->group(['prefix' => 'productGroups'], function() use ($router) {
        $router->get('',  ['uses' => 'ProductGroupController@showAll']);
        $router->get('{id}', ['uses' => 'ProductGroupController@showOne']);
        $router->post('', ['uses' => 'ProductGroupController@create']);
        $router->delete('{id}', ['uses' => 'ProductGroupController@delete']);
        $router->put('{id}', ['uses' => 'ProductGroupController@update']);
        $router->group(['prefix' => '{id}/product'], function() use ($router) {
            $router->post('{product}', ['uses' => 'ProductGroupController@addProduct']);
            $router->delete('{product}', ['uses' => 'ProductGroupController@deleteProduct']);
        });
    });

    $router->group(['prefix' => 'products'], function() use ($router) {
        $router->get('',  ['uses' => 'ProductController@showAll']);
        $router->get('{id}', ['uses' => 'ProductController@showOne']);
        $router->post('', ['uses' => 'ProductController@create']);
        $router->delete('{id}', ['uses' => 'ProductController@delete']);
        $router->put('{id}', ['uses' => 'ProductController@update']);
    });

    $router->group(['prefix' => 'prices'], function() use ($router) {
        $router->get('',  ['uses' => 'PriceController@showAll']);
        $router->get('{id}', ['uses' => 'PriceController@showOne']);
        $router->post('', ['uses' => 'PriceController@create']);
        $router->delete('{id}', ['uses' => 'PriceController@delete']);
        $router->put('{id}', ['uses' => 'PriceController@update']);
    });


    /* we won't be adding any user-routes, as this is more of a placeholder for now*/
    $router->group(['prefix' => 'user'], function() use ($router) {

    });
});
